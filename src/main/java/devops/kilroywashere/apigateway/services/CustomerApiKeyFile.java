package devops.kilroywashere.apigateway.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import devops.kilroywashere.apigateway.models.ClientApiKey;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@Service
public class CustomerApiKeyFile {
  private String customerApiKeysFile = "customerkeys/client-api-keys.json";
  private long customerApiKeysFileLength = 0;

  private List<ClientApiKey> clientApiKeys;

  public void loadApiKeys() {
    try {
      // create object mapper instance
      ObjectMapper mapper = new ObjectMapper();

      File file = Paths.get(customerApiKeysFile).toFile();
      // Get lastModified
      customerApiKeysFileLength = file.length();
      // convert JSON array to list of books
      clientApiKeys = Arrays.asList(mapper.readValue(file, ClientApiKey[].class));

      // TODO : remove test
      System.out.format("****** Loaded keys : \n");
      for (ClientApiKey clientApiKey : clientApiKeys) {
        System.out.format("\troute=%s, client=%s \n", clientApiKey.getRouteId(), clientApiKey.getConsumerId());
      }
    } catch (Exception ex) {
      System.err.printf("d.k.a.s.CustomerApiKeyFile %s", ex.getMessage());
    }
  }

  @Scheduled(fixedDelay = 60000)
  public void scheduleReloadFileTask() {
    // Get lastModified : toutes les minutes
    File file = Paths.get(customerApiKeysFile).toFile();
    if (file.length() != customerApiKeysFileLength) {
      System.out.println("CustomerApiKeyFile.loadApiKeys called");
      loadApiKeys();
    }
  }

  public ClientApiKey findCustomerByKey(String apikey) {
    return clientApiKeys.stream()
            .filter(e -> e.getConsumerKey().equals(apikey))
            .findFirst().orElse(null);
  }
}
