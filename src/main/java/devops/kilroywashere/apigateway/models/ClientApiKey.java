package devops.kilroywashere.apigateway.models;

public class ClientApiKey {
    private String routeId;
    private String consumerId;
    private String consumerKey;

    public ClientApiKey() {}

    public ClientApiKey(String routeId, String consumerId, String consumerKey) {
        this.routeId = routeId;
        this.consumerId = consumerId;
        this.consumerKey = consumerKey;
    }

    public String getRouteId() {
        return routeId;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

}
