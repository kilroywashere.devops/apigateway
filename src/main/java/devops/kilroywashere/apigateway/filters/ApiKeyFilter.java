package devops.kilroywashere.apigateway.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import devops.kilroywashere.apigateway.models.ClientApiKey;
import devops.kilroywashere.apigateway.services.CustomerApiKeyFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.GatewayFilterFactory;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class ApiKeyFilter implements GatewayFilterFactory {

  @Value("${apigateway.key.rentahouse:}")
  private String apiKeyRentAHouse = "";

  private final CustomerApiKeyFile customerApiKeyFile;
  private final Logger logger =
          LoggerFactory.getLogger(ApiKeyFilter.class);

  public ApiKeyFilter(CustomerApiKeyFile customerApiKeyFile) {
    this.customerApiKeyFile = customerApiKeyFile;
    this.customerApiKeyFile.loadApiKeys();
  }

  @Override
  public GatewayFilter apply(Object config) {
    return (exchange, chain) -> {
      logger.info("First global filter");

      Route route = exchange.getAttribute(ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR);
      String routeId = route.getId();
      logger.info("Route: {}", routeId);

      List<String> apiKeyHeader = exchange.getRequest().getHeaders().get("apikey");
      String consummer = "";
      if (CollectionUtils.isEmpty(apiKeyHeader) ||
              (consummer = checkApikey(routeId, apiKeyHeader.get(0))) == null) {
        logger.info("APIKEY not found or invalid");
        throw new ResponseStatusException(
                HttpStatus.UNAUTHORIZED,
                "APIKEY not found or invalid");
      }
      logger.info("APIKEY valid for {}", consummer);

      return chain.filter(exchange);
    };
  }


  @Override
  public Class<Config> getConfigClass() {
    return Config.class;
  }

  @Override
  public Config newConfig() {
    return new Config("GlobalFilter");
  }

  public static class Config {

    public Config(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    private String name;
  }


  private String checkApikey(String routeId, String apikey) {
    // Vérification de l'appel par les WS de RentAHouse
    if (apikey.equals(apiKeyRentAHouse)) {
      logger.info("RentAHouse key used");
      return "RentAHouse";
    }

    ClientApiKey clientApiKey = customerApiKeyFile.findCustomerByKey(apikey);
    if (clientApiKey == null ) {
      logger.error("APIKEY not found");
      return null;
    }

    logger.info("- Key found in json file");
    String customerRouteId = clientApiKey.getRouteId();
    if (customerRouteId.equals("*")) {
      logger.info("- customerId {} access granted to all route", clientApiKey.getConsumerId());
      // Accès à toutes les routes
      return clientApiKey.getConsumerId();
    } else if (customerRouteId.equals(routeId)) {
      logger.info("- customerId {} access granted to route {}", clientApiKey.getConsumerId(), routeId);
      return clientApiKey.getConsumerId();
    }

    logger.error("- customerId {} access is not registered for route {}", clientApiKey.getConsumerId(), routeId);
    return null;
  }
}
