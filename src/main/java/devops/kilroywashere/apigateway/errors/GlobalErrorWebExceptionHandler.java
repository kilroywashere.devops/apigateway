package devops.kilroywashere.apigateway.errors;

import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import java.util.Map;

// Voir https://stackoverflow.com/questions/66254535/spring-cloud-gateway-global-exception-handling-and-custom-error-response

@Component
@Order(-2)
public class GlobalErrorWebExceptionHandler extends
        AbstractErrorWebExceptionHandler {

  public GlobalErrorWebExceptionHandler(ErrorAttributes ea, ApplicationContext applicationContext,
                                        ServerCodecConfigurer serverCodecConfigurer) {
    super(ea, new WebProperties.Resources(), applicationContext);
    super.setMessageWriters(serverCodecConfigurer.getWriters());
    super.setMessageReaders(serverCodecConfigurer.getReaders());
  }

  // constructors

  @Override
  protected RouterFunction<ServerResponse> getRoutingFunction(
          ErrorAttributes errorAttributes) {

    return RouterFunctions.route(
            RequestPredicates.all(), this::renderErrorResponse);
  }

  private Mono<ServerResponse> renderErrorResponse(
          ServerRequest request) {

    Map<String, Object> errorPropertiesMap = getErrorAttributes(request,
            ErrorAttributeOptions.of(ErrorAttributeOptions.Include.MESSAGE));
    errorPropertiesMap.remove("requestId");
    HttpStatus status = HttpStatus.valueOf((Integer) errorPropertiesMap.get("status"));

    return ServerResponse.status(status)
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(errorPropertiesMap));
  }
}
